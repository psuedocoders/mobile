# Location sync plugin for the Schoolsmart Application

### Requirements
* nodejs

### Setup
* Add this line to the android manifest file inside the application tag:
<service android:name="com.pseudocoders.FirebaseLocationSyncService" android:exported="false"/>
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_COURSE_LOCATION" />
