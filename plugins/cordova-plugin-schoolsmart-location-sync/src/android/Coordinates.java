package com.pseudocoders;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by james on 14/09/16.
 */
@IgnoreExtraProperties
public class Coordinates {
    public float latitude;
    public float longitude;

    public Coordinates(){}
}
