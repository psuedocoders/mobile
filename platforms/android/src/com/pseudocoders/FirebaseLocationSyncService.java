package com.pseudocoders;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * A class representing a background service that listen to a Firebase database
 * for requests for the user's current location.
 */
public class FirebaseLocationSyncService extends IntentService implements LocationListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{

    private GoogleApiClient googleApiClient;

    public FirebaseLocationSyncService() {
        super("FirebaseLocationSyncService");
    }

    @Override
    protected void onHandleIntent(Intent workIntent) {
        this.googleApiClient = new GoogleApiClient.Builder(this).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).
                addApi(LocationServices.API).
                build();
        this.googleApiClient.connect();

        // Check if this device is the primary device for the user, if so, start the background service
        final String guardianId = workIntent.getStringExtra("guardianId");
        if(guardianId != null) {
            final DatabaseReference guardianReference = FirebaseDatabase.
                    getInstance().
                    getReference().
                    child("users").
                    child(guardianId);
            guardianReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.child("primaryDeviceId").getValue() != null &&
                            dataSnapshot.child("primaryDeviceId").getValue().
                                    equals(FirebaseInstanceId.getInstance().getId())) {
                        // This is the primary device
                        start(guardianId);
                    } else if (dataSnapshot.child("primaryDeviceId").getValue() == null ||
                            dataSnapshot.child("primaryDeviceId").getValue().equals("")) {
                        // No primary id set, use the current device
                        guardianReference.child("primaryDeviceId").setValue(FirebaseInstanceId.getInstance().getId());
                        start(guardianId);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });
        }
    }

    // Starts listening for database changes and responds with the user's current
    // location, if available
    // Should use an internal unique
    private void start(String guardianId) {
        final DatabaseReference guardianReference = FirebaseDatabase.
        getInstance().
        getReference().
        child("users").
        child(guardianId);

    ValueEventListener guardianListener = new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
          dataSnapshot.getValue();
          Guardian guardian = dataSnapshot.getValue(Guardian.class);
          if (guardian.latestMessage != null &&
                  guardian.latestMessage.type.equals(GuardianLatestMessage.TYPE_PICKUP)) {
              // Update the kiosk with the current location of the user
              final DatabaseReference kioskReference = FirebaseDatabase.
                      getInstance().
                      getReference().
                      child("kiosks").
                      child(guardian.latestMessage.kioskId);

              kioskReference.addListenerForSingleValueEvent(new ValueEventListener() {

                  @Override
                  public void onDataChange(DataSnapshot dataSnapshot) {
                      if (googleApiClient.hasConnectedApi(LocationServices.API)) {
                          Location lastKnownLocation = this.getLastKnownLocation();

                          if(lastKnownLocation != null) {

                              Kiosk kiosk = dataSnapshot.getValue(Kiosk.class);
                              kiosk.coordinates = dataSnapshot.
                                      child("coordinates").
                                      getValue(Coordinates.class);

                              // Send a message to the target kiosk
                              KioskLatestMessage newKioskMessage = new KioskLatestMessage();
                              newKioskMessage.type = KioskLatestMessage.TYPE_LOCATION;
                              newKioskMessage.status = KioskLatestMessage.STATUS_NEW;
                              newKioskMessage.coordinates = new Coordinates(
                                      String.valueOf(lastKnownLocation.getLatitude()),
                                      String.valueOf(lastKnownLocation.getLongitude()));
                              kioskReference.child("latestMessage").setValue(newKioskMessage);

                              // Mark the guardian message we just attended to as old
                              guardianReference.child("latestMessage").
                                      child("status").
                                      setValue(GuardianLatestMessage.STATUS_OLD);
                          }
                      }
                  }

                  public Location getLastKnownLocation() {
                      Location lastKnownLocation = null;
                      try {
                          lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                      } catch (SecurityException e) {
                          System.out.println(e.getMessage());
                      }

                      return lastKnownLocation;
                  }

                  @Override
                  public void onCancelled(DatabaseError databaseError) {}
              });
          }
      }

        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };
    guardianReference.addValueEventListener(guardianListener);
  }

    @Override
    public void onLocationChanged(Location location) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onConnected(@Nullable Bundle bundle) {}

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}
}
