/*global cordova, module*/

module.exports = {
    start: function (guardianId, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "LocationSync", "start", [guardianId]);
    }
};
